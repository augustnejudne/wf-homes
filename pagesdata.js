const graphql = require('gatsby').graphql;
module.exports = [
  {
    title: `Two-story Transformation`,
    tags: ['basementsuite', 'renovation', 'abbotsfor'],
    image: `thorne-renovation-collage.png`,
    body: `
    <h1>Utilizing existing space and maximizing it's income potential.</h1>
    <p>This project was a full basement suite and main floor renovation to an originally custom, high end built home from the 1990's. Our client wanted a legal rental suite in the basement and an open concept in the rest of the home. The main design challenge with the basement rental suite was the small footprint we had to work with, but after many layout revisions, we were able to frame in and finish two bedrooms, full laundrey, one bathroom, dining room, and spacious living room.</p>
<p>The main floor renovations featured a new media room off the kitchen, laundry and mud-room relocation, kitchen makeover with large central island, and open concept throughout. This project was especially memorable because our client was a textile artist and made the final decorating touches make the renovation work look absolutely beautiful.</p>`,
  },
  {
    title: `Repurposed Room Reform`,
    tags: ['renovation', 'chilliwack'],
    image: `lowel-renovation-collage.png`,
    body: `
    <h1>Creating a single living space for multiple purposes.</h1>
    <p>This project was an extensive four level split basement and family room renovation. Our clients had recently purchased the home and wanted to get started renovating right away. Their first desire was to build the unfinished basement into three rooms. The first area would house an exercising space, zero barrier shower, and custom wet dry sauna. The second area would be purposed as a versatile sitting room. The third area included an arts and crafts room with custom cabinetry and sink.</p>
<p>The family room renovation on the main floor featured a wood- burning fireplace with custom surround tile built to the ceiling. Custom cabinetry was also built especially to accommodate the client’s library and entertainment center. The renovation was complete for the clients when we were able to make space for their snooker table.</p>`,
  },
  {
    title: `Facelift for a Tired Kitchen`,
    tags: ['kitchen', 'renovation', 'yarrow'],
    image: `cooke-renovation-collage.png`,
    body: `
    <h1>Giving a dated kitchen the fresh update it deserves.</h1>
    <p>This project was a full kitchen renovation complete with new paint, flooring and trims. This client simply needed an update to her already open spaced home so we spent a lot of time and care on the decorating details. The replaced flooring chosen was a 4” mahogany hardwood product laid throughout the upper floor.</p>
<p>The kitchen cabinets were replaced with custom traditional shaker style profile and to show off some of her finer pieces of China and ceramics we installed custom lighting to the top of the cabinets with glass doors. We were also able to retain the original rare blue granite countertop and find the origin of the granite to fill in the new space. The client’s choice of side stringer millwork was then added to the walls to make the stairs flow with the design of the home and finish off this transformation beautifully.</p>`,
  },
  {
    title: `Upper Level Culinary Creation`,
    tags: ['renovation', 'abbotsford'],
    image: `whatcom-renovation-collage.png`,
    body: `
    <h1>Personalized cabinetry truly transforms a home into a sanctuary.</h1>
    <p>This project was a full kitchen and upper level renovation. Our client is a culinary hobbyist and has a taste for French provincial architecture so he wanted something unique, elegant and subtle all at the same time. For the kitchen he chose hidden appliances and we were able to custom make fridge door handles with our metal fabricator and powder coating trades.</p>
<p>A customized tint was created for the new stair railing and patio door we built while the walls were repainted to tie the upper level together with the kitchen. We installed a polished slate for flooring and on the face of the wide angle glass fireplace. What really made this project special was being able to custom build a cabinet to house our client’s extensive record collection and sample some of the music while we were there.</p>`,
  },
  {
    title: `Modernized Kitchen Makeover`,
    tags: ['renovation', 'abbotsford'],
    image: `rachar-renovation-collage.png`,
    body: `
    <h1>Modern and clean cut design propelled this kitchen into the future.</h1>
    <p>This project was a kitchen renovation for a client who owned a condo. The kitchen cabinets were originally a beautiful stained maple with shaker doors and small crown surround. There was nothing wrong with the cabinets but our client has a taste for tight clean lines and an ultra modern style which she wanted reflected in the kitchen.</p>
<p>The transformation that resulted was a two-tone finish of high gloss white and gray cabinets, LED lighting throughout, and quaint pendant lighting to break up the flow. One of the great pleasures in this business is to help a client realize their dream renovation and enjoy the before and after process with them.</p>`,
  },
];
