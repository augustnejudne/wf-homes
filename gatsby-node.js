/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `projects` });
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    });
  }
};

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            rawMarkdownBody
            frontmatter {
              title
              tags
              quote
              name
              loc
              featuredImage {
                childImageSharp {
                  fluid {
                    base64
                    tracedSVG
                    aspectRatio
                    src
                    srcSet
                    srcWebp
                    srcSetWebp
                    sizes
                    originalImg
                    originalName
                    presentationWidth
                    presentationHeight
                    __typename
                  }
                }
              }
              image {
                childImageSharp {
                  fluid(maxWidth: 800) {
                    base64
                    tracedSVG
                    aspectRatio
                    src
                    srcSet
                    srcWebp
                    srcSetWebp
                    sizes
                    originalImg
                    originalName
                    presentationWidth
                    presentationHeight
                    __typename
                  }
                }
              }
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  const edges = result.data.allMarkdownRemark.edges;

  result.data.allMarkdownRemark.edges.forEach(({ node }, i) => {
    createPage({
      path: '/work' + node.fields.slug,
      component: path.resolve(`./src/templates/projects-template.js`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        slug: node.fields.slug,
        frontmatter: node.frontmatter,
        body: node.rawMarkdownBody,
        prev: i === 0 ? null : edges[i - 1].node,
        next: i === edges.length - 1 ? null : edges[i + 1].node,
      },
    });
  });
};
