const data = require('./pagesdata');
const fs = require('fs');

data.forEach(d => {
  const tags = d.tags.map(tag => tag).join(' ');
  const body = d.body.replace(/\n/g, '');
  const writeThis = `---
title: ${d.title}
tags: ${tags}
featuredImage: ../images/${d.image}
---
${body}
`;
  fs.writeFile(
    `./src/projects/${d.title.toLowerCase().replace(/\s/g, '-')}.md`,
    writeThis,
    function(err) {
      if (err) return console.log('err', err);
      console.log('success');
    }
  );
});

// tags: ${d.tags.split(' ')}
