import React from 'react';

const Test = props => {
  console.log('============================');
  console.log('props');
  console.log('props', props);
  console.log('============================');
  return (
    <div>
      <p>Test works</p>
    </div>
  );
};

export default React.memo(Test);
