import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import Layout from '../components/layout';
import Hero from '../components/hero';
import classes from './styles.module.scss';
import Quote from '../components/quote';
import Img from 'gatsby-image';
import Link from 'gatsby-link';

const ProjectsTemplate = props => {
  const {
    pathContext: {
      frontmatter: { loc, name, quote, tags, title, image, featuredImage },
      body,
      next,
      prev,
    },
  } = props;
  const prevSlug = prev && prev.fields.slug;
  const nextSlug = next && next.fields.slug;
  return (
    <Layout>
      <h1>test</h1>
      <Hero title={title} tags={tags.split(' ')} />
      <div className={classes.imageContainer}>
        {featuredImage && <Img fluid={featuredImage.childImageSharp.fluid} />}
      </div>
      <div className={classes.bodyContainer}>{ReactHtmlParser(body)}</div>
      {quote && (
        <Quote
          details={{ name, loc, quote, image: image.childImageSharp.fluid }}
        />
      )}
      <div className={classes.projectsNav}>
        <p>
          {prevSlug ? (
            <Link to={`/work/${prevSlug}`}>&lt; Previous Project</Link>
          ) : (
            <span>&lt; Previous Project</span>
          )}
        </p>
        <p className={classes.title}>{title}</p>
        <p>
          {nextSlug ? (
            <Link to={`/work/${nextSlug}`}>&gt; Next Project</Link>
          ) : (
            <span>&gt; Next Project</span>
          )}
        </p>
      </div>
    </Layout>
  );
};

export default React.memo(ProjectsTemplate);
