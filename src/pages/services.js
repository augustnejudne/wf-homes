import React from 'react';
import Layout from '../components/layout';
import Hero from '../components/hero';
import Cards from '../components/services/cards';

const Services = () => {
  return (
    <Layout>
      <Hero
        title="Always at your service"
        desc="From project exploration to design concepts to finishing touches, we handle every aspect of the home renovation process. With everything under one roof, we set an extremely high bar for quality and efficiency."
      />
      <Cards />
    </Layout>
  );
};

export default React.memo(Services);
