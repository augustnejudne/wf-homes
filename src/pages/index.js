import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';

import Layout from '../components/layout';
import Hero from '../components/index/hero';
import Gallery from '../components/index/gallery';
import Vision from '../components/index/vision';
import Quote from '../components/quote';

import SEO from '../components/seo';

const IndexPage = () => {
  const query = useStaticQuery(graphql`
    query {
      quoteImage: file(relativePath: { eq: "cheryl-paterson.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  const quoteDetails = {
    name: `Cheryl Paterson`,
    loc: `Abbotsford, BC`,
    quote: `WF Design + Build managed my townhouse renovations and everything was done speedily and on budget. I would absolutely recommend them to anyone.`,
    image: query.quoteImage.childImageSharp.fluid,
  };

  return (
    <Layout>
      <SEO title="Home" />
      <Hero />
      <Gallery />
      <Vision />
      <Quote details={quoteDetails} />
    </Layout>
  );
};

export default IndexPage;
