import React from 'react';
import ContactForm from '../components/contact-form';
import Layout from '../components/layout';

const Contact = () => {
  return (
    <Layout noPreFooter>
      <ContactForm />
    </Layout>
  );
};

export default React.memo(Contact);
