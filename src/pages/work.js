import React from 'react';
import Layout from '../components/layout';
import Hero from '../components/work/hero';
import Gallery from '../components/work/gallery';

const Work = props => {
  return (
    <Layout>
      <Hero />
      <Gallery />
    </Layout>
  );
};

export default React.memo(Work);
