import React from 'react';
import Layout from '../components/layout';
import Hero from '../components/hero';
import Stats from '../components/about/stats';
import Promises from '../components/about/promises';
import CoreValues from '../components/about/core-values';
import MagicMiddle from '../components/about/magic-middle';
import Quote from '../components/quote';
import { useStaticQuery, graphql } from 'gatsby';

const About = props => {
  const query = useStaticQuery(graphql`
    query {
      matt: file(relativePath: { eq: "matt-farley.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  return (
    <Layout>
      <Hero
        title="Building is in our DNA"
        desc="For more than twenty years we've been working in the home renovation space. It's not just what we do — it's who we are."
      />
      <Stats />
      <Promises />
      <CoreValues />
      <MagicMiddle />
      <Quote
        details={{
          name: 'Matt Farley',
          loc: 'Chilliwack, BC',
          quote: `Warren and his team always went the extra mile and were great to work with. We made the right decision working with them, and we'll definitely do it again.`,
          image: query.matt.childImageSharp.fluid,
        }}
      />
    </Layout>
  );
};

export default React.memo(About);
