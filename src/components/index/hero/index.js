import React from 'react';
import classes from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import Link from 'gatsby-link';

const Hero = () => {
  return (
    <div className={classes.container}>
      <div className={classes.textContainer}>
        <h1>
          We create better spaces for you to live <span>+</span> play.
        </h1>
        <p>
          For over a decade, we&apos;ve provided Fraser Valley homeowners like
          you with high-end, custom renovations and additions.
        </p>
        <Link to="/work">
          <button>
            See our work
            <FontAwesomeIcon icon={faArrowRight} />
          </button>
        </Link>
      </div>
    </div>
  );
};

export default React.memo(Hero);
