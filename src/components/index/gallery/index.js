import React from 'react';
import classes from './styles.module.scss';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import Link from 'gatsby-link';

const Gallery = () => {
  const query = useStaticQuery(graphql`
    query {
      image1: file(relativePath: { eq: "index-gallery-1.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image2: file(relativePath: { eq: "index-gallery-2.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image3: file(relativePath: { eq: "index-gallery-3.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image4: file(relativePath: { eq: "index-gallery-4.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  const galleryItems = [
    {
      title: 'Total Main Floor Makeover',
    },
    {
      title: 'From Classic to Contemporary',
    },
    {
      title: 'Tasteful Kitchen Transformation',
    },
    {
      title: 'Full-Scale Main Floor Renovation',
    },
  ];

  const images = [];

  galleryItems.forEach((item, i) => {
    const link = item.title.toLowerCase().replace(/\s/g, '-');
    galleryItems[i].link = '/work/' + link;
  });

  for (let [key, value] of Object.entries(query)) {
    images.push(value);
  }

  images.forEach((image, i) => {
    galleryItems[i].image = image;
  });

  return (
    <div className={classes.container}>
      {galleryItems.map((item, i) => {
        return (
          <Link key={i} to={item.link}>
            <div className={classes.imgContainer}>
              <Img objectFit="cover" fluid={item.image.childImageSharp.fluid} />
              <div className={classes.title}>
                <span>
                  {item.title}
                  <FontAwesomeIcon icon={faArrowRight} />
                </span>
              </div>
            </div>
          </Link>
        );
      })}
    </div>
  );
};

export default React.memo(Gallery);
