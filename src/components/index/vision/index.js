import React from 'react';
import classes from './styles.module.scss';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import Link from 'gatsby-link';

const Vision = () => {
  const query = useStaticQuery(graphql`
    query {
      image1: file(relativePath: { eq: "vision-image-1.jpeg" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image2: file(relativePath: { eq: "vision-image-2.jpeg" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image3: file(relativePath: { eq: "vision-image-3.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  const cardItems = [
    {
      title: 'Plan',
      desc: `First, we'll listen, answer your questions, and advise on the overall project scope and budget.`,
      image: query.image1.childImageSharp.fluid,
    },
    {
      title: 'Design',
      desc: `Then, we'll explore what's possible together, and test our decisions to ensure cost alignment.`,
      image: query.image2.childImageSharp.fluid,
    },
    {
      title: 'Build',
      desc: `Finally, we'll execute the plan, and make sure you're aware of progress every step of the way.`,
      image: query.image3.childImageSharp.fluid,
    },
  ];

  return (
    <div className={classes.container}>
      <h1>Vision. Clarity. Results.</h1>
      <p>
        Our client relationships are built on trust, collaboration, and
        transparency. With your ideas and our guidance and resources, the
        possibilities are endless.
      </p>
      <div className={classes.cards}>
        {cardItems.map((item, i) => {
          return (
            <div key={i} className={classes.card}>
              <Img fluid={item.image} />
              <div className={classes.cardText}>
                <h2>{item.title}</h2>
                <p>{item.desc}</p>
              </div>
            </div>
          );
        })}
      </div>
      <Link to='/services'>
        <button>
          See how we work
          <FontAwesomeIcon icon={faArrowRight} />
        </button>
      </Link>
    </div>
  );
};

export default React.memo(Vision);
