import React from 'react';
import PropTypes from 'prop-types';
import Header from './header';
import PreFooter from './pre-footer';
import Footer from './footer';
import classes from './styles.module.scss';

const Layout = props => {
  return (
    <>
      <Header />
      <main>{props.children}</main>
      {!props.noPreFooter && <PreFooter />}
      <Footer />
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
