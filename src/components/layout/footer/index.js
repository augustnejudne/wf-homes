import React from 'react';
import classes from './styles.module.scss';

const Footer = () => {
  return (
    <div className={classes.container}>
      <div>
        <h5>WF Design + Build &copy; {new Date().getFullYear()}</h5>
        <p>
          4432 Wilson Rd.
          <br /> Chilliwack, BC V2R 5C5
        </p>
        <div>
          <a href="">Privacy Policy</a>
        </div>
      </div>
      <div>
        <h5>Join Our Team</h5>
        <p>
          We&apos;re always open to discussing collaboration opportunities with
          other profesional trades. Go ahead and introduce yourself.
        </p>
        <div>
          <a href="">Say hello</a>
        </div>
      </div>
      <div>
        <h5>Get Social</h5>
        <div>
          <a href="">Facebook</a>
        </div>
        <div>
          <a href="">Instagram</a>
        </div>
        <div>
          <a href="">Twitter</a>
        </div>
      </div>
      <div>
        <h5>Contact Us</h5>
        <div>
          <a href="">Send an email</a>
        </div>
        <div>
          <a href="">778-808-3929</a>
        </div>
      </div>
    </div>
  );
};

export default React.memo(Footer);
