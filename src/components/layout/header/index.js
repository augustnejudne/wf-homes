import { Link } from 'gatsby';
import React, { useState, useEffect, useRef } from 'react';
import classes from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

export const useClickOutside = (ref, cb) => {
  useEffect(() => {
    const handleClickOutside = e => {
      if (ref.current && !ref.current.contains(e.target)) return cb(false);
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref]);
};

const Header = props => {
  const floatingMenu = useRef();
  const query = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "wf_logo.png" }) {
        childImageSharp {
          fixed(width: 68) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `);

  const links = [
    {
      to: '/services',
      display: 'Services',
    },
    {
      to: '/work',
      display: 'Work',
    },
    {
      to: '/about',
      display: 'About',
    },
  ];

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const handleScroll = () => {
    let prevScrollPos = window.pageYOffset;

    window.onscroll = function() {
      const currentScrollPos = window.pageYOffset;
      if (prevScrollPos > currentScrollPos) {
        document.getElementById('navbar').style.top = '0';
      } else if (currentScrollPos < 500) {
        document.getElementById('navbar').style.top = '0';
      } else {
        document.getElementById('navbar').style.top = '-97px';
      }
      prevScrollPos = currentScrollPos;
    };
  };

  const [showMenu, set_showMenu] = useState(false);

  const handleMenuIconClick = () => {
    set_showMenu(!showMenu);
  };

  useClickOutside(floatingMenu, handleMenuIconClick);

  return (
    <header>
      <div id="navbar" className={classes.container}>
        <div className={classes.imgContainer}>
          <Link to="/">
            <Img fixed={query.placeholderImage.childImageSharp.fixed} />
          </Link>
        </div>
        <div className={classes.menuIconContainer}>
          <FontAwesomeIcon icon={faBars} onClick={handleMenuIconClick} />
        </div>
        {showMenu && (
          <div ref={floatingMenu} className={classes.floatingMenu}>
            <ul>
              {links.map((l, i) => {
                return (
                  <li key={i}>
                    <Link to={l.to}>{l.display}</Link>
                  </li>
                );
              })}
              <li>
                <Link to="/contact">
                  Start your project
                  <FontAwesomeIcon icon={faArrowRight} />
                </Link>
              </li>
            </ul>
          </div>
        )}
        <ul>
          {links.map((l, i) => {
            return (
              <li key={i}>
                <Link to={l.to}>{l.display}</Link>
              </li>
            );
          })}
          <li className={classes.start}>
            <Link to="/contact">
              Start your project
              <FontAwesomeIcon icon={faArrowRight} />
            </Link>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
