import React from 'react';
import classes from './styles.module.scss';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import Link from 'gatsby-link';

const PreFooter = () => {
  const query = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "pre-footer.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);
  return (
    <div className={classes.container}>
      <div className={classes.imgContainer}>
        <Img fluid={query.placeholderImage.childImageSharp.fluid} />
      </div>
      <div className={classes.text}>
        <h3>
          If you&apos;re eager to start a new home renovation project,
          we&apos;re experienced, established, and ready to get to work.
        </h3>
        <p>
          Since 2005, we&apos;ve helped hundreds of Fraser Valley homeowners
          like you create better living spaces with forward-thinking design,
          experience-driven insight, and expert level craftsmanship.
        </p>
        <Link to='/contact'>
          <button>
            Let&apos;s work together
            <FontAwesomeIcon icon={faArrowRight} />
          </button>
        </Link>
      </div>
    </div>
  );
};

export default React.memo(PreFooter);
