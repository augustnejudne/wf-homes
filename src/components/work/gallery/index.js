import React from 'react';
import classes from './styles.module.scss';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import Link from 'gatsby-link';

const Gallery = () => {
  const query = useStaticQuery(graphql`
    query {
      image1: file(relativePath: { eq: "index-gallery-1.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image2: file(relativePath: { eq: "index-gallery-2.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image3: file(relativePath: { eq: "index-gallery-3.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image4: file(relativePath: { eq: "index-gallery-4.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image5: file(relativePath: { eq: "thorne-renovation-thumbnail.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image6: file(relativePath: { eq: "lowel-renovation-thumbnail.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image7: file(relativePath: { eq: "cooke-renovation-thumbnail.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image8: file(relativePath: { eq: "whatcom-renovation-thumbnail.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image9: file(relativePath: { eq: "rachar-renovation-thumbnail.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  const images = [];
  const galleryItems = [
    {
      title: 'Total Main Floor Makeover',
    },
    {
      title: 'From Classic to Contemporary',
    },
    {
      title: 'Tasteful Kitchen Transformation',
    },
    {
      title: 'Full-Scale Main Floor Renovation',
    },
    {
      title: 'Two-story Transformation',
    },
    {
      title: 'Repurposed Room Reform',
    },
    {
      title: 'Facelift for a Tired Kitchen',
    },
    {
      title: 'Upper Level Culinary Creation',
    },
    {
      title: 'Modernized Kitchen Makeover',
    },
  ];

  galleryItems.forEach((item, i) => {
    const link = item.title.toLowerCase().replace(/\s/g, '-');
    galleryItems[i].link = '/work/' + link;
  });

  for (let [key, value] of Object.entries(query)) {
    images.push(value.childImageSharp.fluid);
  }

  images.forEach((image, i) => {
    galleryItems[i].image = image;
  });

  return (
    <div className={classes.container}>
      {galleryItems.map((item, i) => {
        return (
          <Link key={i} to={item.link}>
            <div className={classes.imgContainer}>
              <Img objectFit="cover" fluid={item.image} />
              <div className={classes.title}>
                <span>
                  {item.title}
                  <FontAwesomeIcon icon={faArrowRight} />
                </span>
              </div>
            </div>
          </Link>
        );
      })}
    </div>
  );
};

export default React.memo(Gallery);
