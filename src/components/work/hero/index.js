import React from 'react';
import classes from './styles.module.scss';

const Hero = props => {


  return (
    <div className={classes.container}>
      <h1>
        Functionally beautiful home renovations <span>+</span> additions
      </h1>
      <p>Expertly crafted with meticulous care and attention to detail.</p>
    </div>
  );
};

export default React.memo(Hero);
