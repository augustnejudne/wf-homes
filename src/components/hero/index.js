import React from 'react';
import classes from './styles.module.scss';

const ServicesHero = ({ title, desc, tags }) => {
  return (
    <div
      className={classes.container}
      style={{ paddingBottom: tags ? '16rem' : '12rem' }}
    >
      <h1>{title}</h1>
      {desc && <p>{desc}</p>}
      {tags && (
        <div className={classes.tagsContainer}>
          {tags &&
            tags.map((tag, i) => {
              return (
                <div key={i} className={classes.tag}>
                  #{tag.toUpperCase()}
                </div>
              );
            })}
        </div>
      )}
    </div>
  );
};

export default React.memo(ServicesHero);
