import React from 'react';
import classes from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuoteLeft } from '@fortawesome/free-solid-svg-icons';
import Img from 'gatsby-image';

const Quote = ({ details: { name, loc, quote, image } }) => {
  return (
    <div className={classes.container}>
      <FontAwesomeIcon icon={faQuoteLeft} className={classes.quoteIcon} />
      <p className={classes.quote}>{quote}</p>
      <div className={classes.person}>
        <div className={classes.imgContainer}>
          {image && <Img fluid={image} />}
        </div>
        <div className={classes.text}>
          <p className={classes.name}>{name}</p>
          <p className={classes.location}>{loc}</p>
        </div>
      </div>
    </div>
  );
};

export default React.memo(Quote);
