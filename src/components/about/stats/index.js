import React from 'react';
import classes from './styles.module.scss';

const Stats = props => {
  const stats = [
    {
      title: '2005',
      sub: 'year founded',
    },
    {
      title: '300+',
      sub: 'completed projects',
    },
    {
      title: '98.7%',
      sub: 'client satisfaction',
    },
    {
      title: '75K+',
      sub: 'screws used',
    },
  ];

  return (
    <div className={classes.container}>
      {stats.map((s, i) => {
        return (
          <div key={i} className={classes.stat}>
            <h1>{s.title}</h1>
            <p>{s.sub}</p>
          </div>
        );
      })}
    </div>
  );
};

export default React.memo(Stats);
