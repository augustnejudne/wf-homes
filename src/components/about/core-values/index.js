import React from 'react';
import classes from './styles.module.scss';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

const CoreValues = () => {
  const query = useStaticQuery(graphql`
    query {
      image1: file(relativePath: { eq: "values-right.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image2: file(relativePath: { eq: "values-together.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      image3: file(relativePath: { eq: "values-purpose.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  const cardItems = [
    {
      title: 'We do it right',
      desc: `There's never a stressful amount of pressure to meet unreasonable expectations. We project-manage towards specific quality goals to ensure the best possible result.`,
      image: query.image1.childImageSharp.fluid,
    },
    {
      title: 'We do it together',
      desc: `Collaboration and open lines of communication is an important part of our process. We choose to work with responsible homeowners and trades people we can trust.`,
      image: query.image2.childImageSharp.fluid,
    },
    {
      title: 'We do it with purpose',
      desc: `Building close relationships with our clients and turning dreams into reality is a privilege. It's the reason we do what we do and why we enjoy it so much.`,
      image: query.image3.childImageSharp.fluid,
    },
  ];

  return (
    <div className={classes.container}>
      <h1>Core Values</h1>
      <p>
        We are a mission-driven company committed to transparency,
        accountability, sustainability and positive impact. It’s not just good
        business – it’s the only way we want to do business.
      </p>
      <div className={classes.cards}>
        {cardItems.map((item, i) => {
          return (
            <div key={i} className={classes.card}>
              <Img fluid={item.image} />
              <div className={classes.cardText}>
                <h2>{item.title}</h2>
                <p>{item.desc}</p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default React.memo(CoreValues);
