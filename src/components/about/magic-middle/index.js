import React from 'react';
import classes from './styles.module.scss';
import magicMiddleImg from '../../../images/wfdb_magic_middle.svg';

const MagicMiddle = props => {
  return (
    <div className={classes.container}>
      <div className={classes.promise}>
        <h2>The magic middle</h2>
        <p>
          In our industry, business is often won or lost on your reputation.
          That&apos;s why we&apos;re upstanding citizens, hard workers, and thoughtful
          people.
        </p>
        <p>
          We believe in a client-first approach, and take great care to ensure
          the homeowners we work with have an exceptional experience.
        </p>
      </div>
      <div className={classes.imageContainer}>
        <img src={magicMiddleImg} />
      </div>
    </div>
  );
};

export default React.memo(MagicMiddle);
