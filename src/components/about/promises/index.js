import React from 'react';
import classes from './styles.module.scss';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

const Promises = props => {
  const query = useStaticQuery(graphql`
    query {
      warren: file(relativePath: { eq: "wf_warren.png" }) {
        childImageSharp {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  return (
    <div className={classes.container}>
      <div className={classes.imageContainer}>
        <Img fluid={query.warren.childImageSharp.fluid} />
      </div>
      <div className={classes.promise}>
        <h2>Our promise</h2>
        <p>
          We believe in people over profit and strive to operate our business
          with integrity and trust. From clients to staff, our strongest assets
          and greatest resources are the relationships we build.
        </p>
        <p>
          When you choose to work with us, you&apos;re not just choosing a
          general contractor. You&apos;re choosing residential design + build
          experts who care.
        </p>
      </div>
    </div>
  );
};

export default React.memo(Promises);
