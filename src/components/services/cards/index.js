import React from 'react';
import classes from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilRuler } from '@fortawesome/free-solid-svg-icons';
import { faDraftingCompass } from '@fortawesome/free-solid-svg-icons';
import { faSwatchbook } from '@fortawesome/free-solid-svg-icons';
import { faHardHat } from '@fortawesome/free-solid-svg-icons';

const ServicesCards = () => {
  const cards = [
    {
      title: 'Custom design + build',
      icon: faPencilRuler,
      desc: `From the beginning, we listen, brainstorm, and discuss ideas to bring out the full potential of spaces and areas that are important to you. This step informs and educates you on what project ideas are possible and what budget you will need to complete them.`,
    },
    {
      title: 'Architecture + engineering',
      icon: faDraftingCompass,
      desc: `From ideas to drawn plans, we present the project in printed form with the help of our architect. Once the final draft is representative of your goals, we move on to engineering. And if building permit applications are required, we'll take care of them for you.`,
    },
    {
      title: 'Interior + exterior design',
      icon: faSwatchbook,
      desc: `This final stage is all about attention to detail. Our designer will walk you through finishing selections, color coordination, window design, flooring, trim, and much much more. It's easy to feel overwhelmed, but we're here to help make it stress free and fun.`,
    },
    {
      title: 'Project management',
      icon: faHardHat,
      desc: `Once you give the green light on all the planning and design decisions, we're ready to start building. We supply and operate all trades, materials and supplies, as well as site operations. Our goal is always to keep the project on time and on budget.`,
    },
  ];
  return (
    <div className={classes.container}>
      {cards.map((card, i) => {
        return (
          <div key={i} className={classes.card}>
            <div className={classes.iconContainer}>
              <div className={classes.yellowBox}/>
              <FontAwesomeIcon icon={card.icon} />
            </div>
            <h2>{card.title}</h2>
            <p>{card.desc}</p>
          </div>
        );
      })}
    </div>
  );
};

export default React.memo(ServicesCards);
