import React from 'react';
import classes from './styles.module.scss';

const ContactForm = props => {
  return (
    <div className={classes.container}>
      <h1>Let&apos;s build something great together</h1>
      <p>
        Whatever your ambition, we&apos;d love to design and build your next big
        home addition or lend a hand with a major renovation.
      </p>
      <div className={classes.col2}>
        <label>
          Name
          <input type="text" required />
        </label>
        <label>
          Email address
          <input type="email" required />
        </label>
      </div>
      <div className={classes.col2}>
        <label>
          Phone number
          <input type="phone" required />
        </label>
        <label>
          Physical address (optional)
          <input type="text" />
        </label>
      </div>
      <div className={classes.col2}>
        <label>
          How did you hear about us?
          <input type="text" required />
        </label>
        <label>
          What is your budget?
          <select>
            <option>Select one</option>
            <option>Less than $100,000</option>
            <option>$100,000 - $250,000</option>
            <option>$250,000 - $500,000</option>
            <option>More than $500,000</option>
          </select>
        </label>
      </div>
      <div className={classes.col1}>
        <label>
          How can we help?
          <textarea id="" name="" rows="10"></textarea>
        </label>
      </div>
      <button>Submit</button>
    </div>
  );
};

export default React.memo(ContactForm);
