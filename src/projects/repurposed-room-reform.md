---
title: Repurposed Room Reform
tags: renovation chilliwack
featuredImage: ../images/lowel-renovation-collage.png
---
    <h1>Creating a single living space for multiple purposes.</h1>    <p>This project was an extensive four level split basement and family room renovation. Our clients had recently purchased the home and wanted to get started renovating right away. Their first desire was to build the unfinished basement into three rooms. The first area would house an exercising space, zero barrier shower, and custom wet dry sauna. The second area would be purposed as a versatile sitting room. The third area included an arts and crafts room with custom cabinetry and sink.</p><p>The family room renovation on the main floor featured a wood- burning fireplace with custom surround tile built to the ceiling. Custom cabinetry was also built especially to accommodate the client’s library and entertainment center. The renovation was complete for the clients when we were able to make space for their snooker table.</p>
