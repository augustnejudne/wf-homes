---
title: Modernized Kitchen Makeover
tags: renovation abbotsford
featuredImage: ../images/rachar-renovation-collage.png
---
    <h1>Modern and clean cut design propelled this kitchen into the future.</h1>    <p>This project was a kitchen renovation for a client who owned a condo. The kitchen cabinets were originally a beautiful stained maple with shaker doors and small crown surround. There was nothing wrong with the cabinets but our client has a taste for tight clean lines and an ultra modern style which she wanted reflected in the kitchen.</p><p>The transformation that resulted was a two-tone finish of high gloss white and gray cabinets, LED lighting throughout, and quaint pendant lighting to break up the flow. One of the great pleasures in this business is to help a client realize their dream renovation and enjoy the before and after process with them.</p>
