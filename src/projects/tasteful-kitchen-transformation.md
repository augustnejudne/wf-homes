---
tags: renovation mission
featuredImage: ../images/schafer-renovation-collage.png
title: Tasteful Kitchen Transformation
---

<h1>The dishes can wait, but life can't.</h1>
<p>This project was a full kitchen and main foyer renovation. The original kitchen had dated cabinetry, a narrow design, dark lighting, and very poorly laid out for functionality. Our client was a culinary arts teacher and wanted to transform her kitchen into a more practical workspace and social atmosphere.</p>
<p>We met these requests by adding a large bay window that overlooked a gorgeous green space in the backyard, centralized the kitchen sink facing the window, built a separate wall oven with microwave insert, and tied it all together with a large cabinet island. Our favorite and final touch to this renovation featured a beautiful custom made cork floating floor that ran throughout the main level.</p>
