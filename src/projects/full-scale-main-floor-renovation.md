---
tags: renovation mission
featuredImage: ../images/lick-renovation-collage.png
title: Full-scale Main Floor Renovation
---

<h1>When in doubt, you can always change the layout.</h1>
<p>Some home renovation projects focus on a single room. Others, like this project, present the challenge of incorporating multiple rooms into a more functional, cohesive living space. We were tasked with renovating a kitchen, dining room, family room, and bathroom while opening up the original closed concept floor plan. After several meetings we were able to help our client entirely change the design on their main floor. These changes resulted in the kitchen being moved to where the original family room was and moving the dining room to where the original kitchen was.</p>
<p>In order to open up the space and improve the ambience, we had to remove a dividing wall, add three new windows, a sliding glass door for patio access, and take out all ceiling drop beams. Our clients were so pleased with the transformation of their main floor renovation that they graciously hosted an open house for our company to showcase the work. It is projects and clients like these that bring so much more meaning and importance to our business.</p>`,
