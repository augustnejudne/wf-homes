---
title: Facelift for a Tired Kitchen
tags: kitchen renovation yarrow
featuredImage: ../images/cooke-renovation-collage.png
---
    <h1>Giving a dated kitchen the fresh update it deserves.</h1>    <p>This project was a full kitchen renovation complete with new paint, flooring and trims. This client simply needed an update to her already open spaced home so we spent a lot of time and care on the decorating details. The replaced flooring chosen was a 4” mahogany hardwood product laid throughout the upper floor.</p><p>The kitchen cabinets were replaced with custom traditional shaker style profile and to show off some of her finer pieces of China and ceramics we installed custom lighting to the top of the cabinets with glass doors. We were also able to retain the original rare blue granite countertop and find the origin of the granite to fill in the new space. The client’s choice of side stringer millwork was then added to the walls to make the stairs flow with the design of the home and finish off this transformation beautifully.</p>
