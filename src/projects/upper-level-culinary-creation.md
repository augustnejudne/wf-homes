---
title: Upper Level Culinary Creation
tags: renovation abbotsford
featuredImage: ../images/whatcom-renovation-collage.png
---
    <h1>Personalized cabinetry truly transforms a home into a sanctuary.</h1>    <p>This project was a full kitchen and upper level renovation. Our client is a culinary hobbyist and has a taste for French provincial architecture so he wanted something unique, elegant and subtle all at the same time. For the kitchen he chose hidden appliances and we were able to custom make fridge door handles with our metal fabricator and powder coating trades.</p><p>A customized tint was created for the new stair railing and patio door we built while the walls were repainted to tie the upper level together with the kitchen. We installed a polished slate for flooring and on the face of the wide angle glass fireplace. What really made this project special was being able to custom build a cabinet to house our client’s extensive record collection and sample some of the music while we were there.</p>
