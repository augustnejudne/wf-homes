---
title: Two-story Transformation
tags: basementsuite renovation abbotsfor
featuredImage: ../images/thorne-renovation-collage.png
---
    <h1>Utilizing existing space and maximizing it's income potential.</h1>    <p>This project was a full basement suite and main floor renovation to an originally custom, high end built home from the 1990's. Our client wanted a legal rental suite in the basement and an open concept in the rest of the home. The main design challenge with the basement rental suite was the small footprint we had to work with, but after many layout revisions, we were able to frame in and finish two bedrooms, full laundrey, one bathroom, dining room, and spacious living room.</p><p>The main floor renovations featured a new media room off the kitchen, laundry and mud-room relocation, kitchen makeover with large central island, and open concept throughout. This project was especially memorable because our client was a textile artist and made the final decorating touches make the renovation work look absolutely beautiful.</p>
