---
tags: renovation addition langley
quote: From the drawing board to the finish line, the attention to detail and quality of the work was exceptional. Working with WF Design + Build was a great experience.
name: Julie Chen
loc: Abbotsford, BC
featuredImage: ../images/chen-renovation-collage.png
image: ../images/julie-chen.png
title: From Classic to Contemporary
---
<h1>Nothing says welcome home like a double sided fireplace.</h1><p>
This project was a complete renovation that was added to an existing home originally built in 1905. The design phase presented some interesting challenges, as the previous owners built a few additions without permits or proper building practices. Additionally, the original home was built too close to a fish bearing creek, which meant if our clients wanted to build new, they would have had to move the build thirty meters away from the creek.
</p>
<p>
After a lot of research and negotiation with the city of Langley, we were able to grandfather in the existing footprint of the home by keeping a small percentage of an original wall and extend the home to 4,500 square feet. Despite the challenges we faced, we were able to fulfill our clients dreams and provide them with a beautiful home featuring a living room with a two sided fireplace, bathroom ensuite featuring a Japanese style soaker tub, two rear patios, and a media room.
</p>`
