---
tags: renovation abbotsford
quote: My wife and I are extremely happy with the new updates WF Design + Build did to our main floor. We now have a great space to entertain and unwind. Thanks again!
name: Derek Rogelsky
loc: Abbotsford, BC
featuredImage: ../images/rogelsky-renovation-collage.png
image: ../images/derek-rogelsky.png
title: Total Main Floor Makeover
---
<h1>Goodbye unwelcoming and awkward. Hello custom and contemporary.</h1>
<p>
It's pretty fun to have the opportunity to co-design a project with your client. Derek and Roxanne Rogelsky had a vision for opening up their main floor with a major kitchen renovation, and we were happy to help. The original home was built on spec by an unknown developer, and the kitchen layout wasn't very practical for foodies. It was cramped, closed off, and not ideal for entertaining or food prep.
</p>
<p>
The biggest hurdle was the large wall that divided the living room from the kitchen – it just took up way too much usable real estate. Unfortunately due to the poor design, a portion of the wall was serving as a major bearing point, so just knocking it down wasn't an option. In most cases, placing an engineered beam would be the perfection solution, but we had to get a bit more creative with our working budget.
</p>
<blockquote>
"When it comes to renovations, if you can't remove something you just turn it into functional art."
</blockquote>
<p>
We decided to build a new island around the existing 12”x 4” bearing post, complete with custom cabinetry and a quartz countertop. We also inset some custom shelves with led lighting and installed some beautiful custom ledge stone to the ceiling. To create a cohesive look and feel, we used the same stone to reface the rebuilt fireplace chase in the living room, and it tied the two rooms together nicely.
</p>
<p>
When the project was wrapped up, the new kitchen was laid out beautifully in a shaker styled finish painted in a neutral grey. We thoughtfully designed the range hood to incorporate a more classy look, and installed new engineered hardwood throughout the main floor. We also added some custom millwork to the lower half of the dining room walls for a bit of extra elegance to an otherwise dark room. Derek and Roxanne were very happy with the end result.
</p>
